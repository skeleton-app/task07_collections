package com.epam.modal;

import java.util.*;

public interface Tree<T> {
    void add(T object);

    boolean contains(T object);

    boolean remove(T object);

    void show();
}
