package com.epam.modal;

import static com.epam.Application.logger;

public class BinaryTree<T> implements Tree {
    private Node root = new Node();

    public void add(Object object) {
        root.add(object);
    }

    public void show() {
        root.show();
    }

    public boolean contains(Object object) {
        return root.contains(object);
    }


    public boolean remove(Object object) {
        return root.remove(object);
    }

    private class Node {
        Node left;
        Node right;
        Object object;

        Node() {
        }

        void add(Object object) {
            if (this.object == null) {
                this.object = object;
            } else if (object.hashCode() < this.object.hashCode()) {
                if (left == null) {
                    left = new Node();
                }
                left.add(object);
            } else if (object.hashCode() > this.object.hashCode()) {
                if (right == null) {
                    right = new Node();
                }
                right.add(object);
            }
        }

        boolean contains(Object object) {
            if (this.object.equals(object)) {
                return true;
            } else if (left != null && this.object.hashCode() > object.hashCode()) {
                return left.contains(object);
            } else if (right != null) {
                return right.contains(object);
            }
            return false;
        }

        private Node returnNode(Object object) {
            Node node = null;
            if (this.object.equals(object)) {
                node = this;
            } else if (left != null && this.object.hashCode() > object.hashCode()) {
                node = left.returnNode(object);
            } else if (right != null) {
                node = right.returnNode(object);
            }
            return node;
        }


        boolean remove(Object object) {
            Node node = returnNode(object);
            if (node == null) {
                return false;
            } else {
                rewriteNode(node);
                calibrate(node);
                return true;
            }
        }

        private void calibrate(Node node) {
            if (node.right != null && node.right.object == null) {
                node.right = null;
            } else {
                calibrate(node.right);

            }
            if (node.left != null && node.left.object == null) {
                node.left = null;
            } else {
                calibrate(node.left);
            }
        }

        private void rewriteNode(Node node) {
            if (node.right == null && node.left == null) {
                node.object = null;
            } else if (node.right != null && node.left == null) {
                node.object = node.right.object;
                rewriteNode(node.right);
            } else if (node.right == null && node.left != null) {
                node.object = node.left.object;
                rewriteNode(node.left);
            } else {
                node.object = node.right.object;
                rewriteNode(node.right);
            }
        }

        void show() {
            logger.info(object + " ");
            if (left != null) {
                left.show();
            }
            if (right != null) {
                right.show();
            }
        }
    }


}
