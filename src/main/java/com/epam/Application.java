package com.epam;

import com.epam.modal.BinaryTree;
import com.epam.modal.Tree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        Tree<Integer> tree = new BinaryTree();
        BinaryTree<Integer> e = (BinaryTree<Integer>) tree;
        Integer integer = new Integer(11);

        Integer integer1 = new Integer(10);
        tree.add(integer);
        tree.add(integer1);
        tree.add(integer1);
        tree.add(4);
        tree.add(6);
        tree.add(3);
        tree.add(2);
        tree.add(22);
        tree.add(17);
        tree.add(15);
        tree.add(16);
        tree.add(14);
        tree.add(10);
        tree.add(22);
        tree.add(31);
        tree.add(33);
        tree.show();
        logger.info(tree.remove(integer1));
        tree.show();

        tree.add(1);
        logger.info("");
        tree.show();
        logger.info(tree.contains(1130));
    }
}
